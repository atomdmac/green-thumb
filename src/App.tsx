import React, { useRef } from 'react';
import './App.css';

import { iterate } from "./tree-builder"
import { createTreeNode, TreeNode } from "./tree-node"
import { TreeConfig } from './tree-config'
import { render } from "./tree-renderer"
import { Point } from "./point"

const root: TreeNode = createTreeNode({
  root: null,
  position: new Point(100, 100)
})

const config: TreeConfig = {
  fertilityRange: [10, 20],
  branchLengthRange: [10, 20],
  branchProbability: 20
}

let tree: TreeNode = iterate(root, config)

const TreeView = (props: any) => {
  const canvasRef = useRef({} as HTMLCanvasElement)

  const renderTree = () => {
    const ctx = canvasRef.current.getContext('2d') as CanvasRenderingContext2D
    ctx.clearRect(0, 0, 500, 500)
    render(props.rootNode, ctx)
  }

  const clearRender = () => {
    const ctx = canvasRef.current.getContext('2d') as CanvasRenderingContext2D
    ctx.clearRect(0, 0, 500, 500)
  }

  const iterateAndRenderTree = () => {
    for (let i = 0; i < 20; i++) {
      tree = iterate(tree, config)
    }
    console.log(tree)
    renderTree()
  }

  return (
    <div>
      <button onClick={() => renderTree()}>Re-Render</button>
      <button onClick={() => clearRender()}>Clear</button>
      <button onClick={() => iterateAndRenderTree()}>Iterate + Render</button>
      <canvas ref={canvasRef} width="500" height="500"></canvas>
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <TreeView rootNode={tree} />
    </div>
  );
}

export default App;
