import { TreeNode } from "./tree-node"

export const render = (n: TreeNode, c: CanvasRenderingContext2D) => {
  const parent = n.parent;
  if (parent) {
    c.strokeStyle = '1px solid black'
    c.moveTo(parent.position.x, parent.position.y)
    c.lineTo(n.position.x, n.position.y)
    c.stroke()
  }

  if (n.children.length) {
    n.children.forEach(cn => render(cn, c))
  }
}
