export interface TreeConfig {
  fertilityRange: number[]
  branchLengthRange: number[]
  branchProbability: number
}
