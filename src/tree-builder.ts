import { Point } from "./point"
import { createTreeNode, TreeNode } from "./tree-node"
import { TreeConfig } from "./tree-config"

export const isRoot = (n: TreeNode) => !n.root

export const iterate = (n: TreeNode, c: TreeConfig): TreeNode => {
  // If this is the root node, sprout a child node
  if (isRoot(n) && n.children.length === 0) {
    n.children.push(createTreeNode({
      children: [],
      root: n,
      parent: n,
      depth: n.depth + 1,
      age: 0,
      direction: new Point(Math.random(), Math.random()), // Grow in random direction
      position:  n.position.clone()
    }))
  }

  if (!isRoot(n)){
    // Grow
    n.position = n.position.sum(n.direction)

    // If this is a child node:
    //  - Should grow outward?
    //  - Should sprout children?
    if (n.parent && n.position.difference(n.parent.position).length > 30 && n.children.length < 1) {
      n.children.push(createTreeNode({
        children: [],
        root: n,
        parent: n,
        depth: n.depth + 1,
        age: 0,
        direction: new Point(Math.random(), Math.random()), // Grow in random direction
        position:  n.position.clone()
      }))
    }
  }


  // Iterate through children
  if (n.children.length > 0) {
    n.children = n.children.map(cn => iterate(cn, c))
  }

  n.age += 1

  return n;
}
