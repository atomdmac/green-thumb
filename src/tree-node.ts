import { Point } from "./point"

export interface TreeNode {
  children: TreeNode[],
  root: TreeNode | null
  parent: TreeNode | null
  depth: number
  age: number
  direction: Point
  position: Point
}

export const createTreeNode = ({
  root = null,
  parent = null,
  children = [],
  depth = 0,
  age = 0,
  direction = new Point(0, 0),
  position = new Point(0, 0),
}: Partial<TreeNode>): TreeNode => ({
    children,
    root,
    parent,
    depth,
    age,
    direction,
    position
})
