export class Point {
  x: number
  y: number

  constructor (x: number = 0, y: number = 0) {
    this.x = x
    this.y = y
  }

  get length() {
    return Math.sqrt(this.x * this.x + this.y * this.y)
  }

  clone() {
    return new Point(this.x, this.y)
  }

  normalize() {
    const l = this.length
    return new Point(
      this.x / l,
      this.y / l
    )
  }

  difference(p: Point) {
    return new Point(
      this.x - p.x,
      this.y - p.y
    )
  }

  sum(p: Point) {
    return new Point(
      this.x + p.x,
      this.y + p.y
    )
  }

  scale(l: number) {
    return new Point(
      this.x * (l / this.length),
      this.y * (l / this.length)
    )
  }

  // TODO: addAngle() {} -> Point
}
